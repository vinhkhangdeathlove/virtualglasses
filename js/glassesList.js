export class GlassesList {
  constructor() {
    this.glist = [];
  }

  addGlasses(glasses) {
    this.glist.push(glasses);
  }

  renderGlasses() {
    let content = "";
    content = this.glist.reduce((glassesContent, item, index) => {
      glassesContent += `
                <div class="col-4">
                    <img class="img-fluid" data-id="${item.id}" onclick="tryOnGlasees(event)" src="${item.src}" alt="Glasses">
                </div>
            `;
      return glassesContent;
    }, "");
    return content;
  }
}
